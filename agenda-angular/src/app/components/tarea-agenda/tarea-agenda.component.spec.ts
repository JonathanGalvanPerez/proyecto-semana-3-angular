import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TareaAgendaComponent } from './tarea-agenda.component';

describe('TareaAgendaComponent', () => {
  let component: TareaAgendaComponent;
  let fixture: ComponentFixture<TareaAgendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TareaAgendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TareaAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
