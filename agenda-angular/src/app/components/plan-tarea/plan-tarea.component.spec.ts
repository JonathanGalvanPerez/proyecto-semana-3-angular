import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanTareaComponent } from './plan-tarea.component';

describe('PlanTareaComponent', () => {
  let component: PlanTareaComponent;
  let fixture: ComponentFixture<PlanTareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanTareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanTareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
