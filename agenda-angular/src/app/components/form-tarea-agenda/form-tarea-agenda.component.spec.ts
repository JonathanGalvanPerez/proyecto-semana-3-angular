import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTareaAgendaComponent } from './form-tarea-agenda.component';

describe('FormTareaAgendaComponent', () => {
  let component: FormTareaAgendaComponent;
  let fixture: ComponentFixture<FormTareaAgendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTareaAgendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTareaAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
