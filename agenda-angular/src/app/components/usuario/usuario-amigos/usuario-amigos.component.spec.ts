import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioAmigosComponent } from './usuario-amigos.component';

describe('UsuarioAmigosComponent', () => {
  let component: UsuarioAmigosComponent;
  let fixture: ComponentFixture<UsuarioAmigosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsuarioAmigosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioAmigosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
